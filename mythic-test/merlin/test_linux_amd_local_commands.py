import os
import pytest
from mythic import mythic
from uuid import uuid4



@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_ls(mythic_instance, task_timeout, make_callback_display_id):
    async for callback_id in make_callback_display_id(
            use_payload_build_file="merlin_build.json",
            # use_payload_uuid="4faadb9c-4a3c-4c6f-b4f8-343965335a29",
            remote_execution_function=None,#wrapped_remote_execution_function,
            remote_cleanup_function=None#helper_functions.remote_cleanup_function
    ):
        # issue the task and wait for it to finish
        task = await mythic.issue_task(
            mythic=mythic_instance,
            command_name="ls",
            parameters="",
            callback_display_id=callback_id,
            token_id=None,
            wait_for_complete=True,
            custom_return_attributes="""
                id
                status
                completed
                display_id
                callback {
                    id
                }
            """,
            timeout=task_timeout
        )
        # gather all the task's output
        output = await mythic.waitfor_for_task_output(
            mythic=mythic_instance,
            task_display_id=task["display_id"],
            timeout=task_timeout,
        )
        # assert that the output is as expected
        assert b"Directory listing" in output
        


use_payload_build_file = "merlin_build.json"
remote_execution_function = None #wrapped_remote_execution_function
remote_cleanup_function = None #helper_functions.remote_cleanup_function


# use a module-wide instance of a callback, callback_display_id, based on the above module parameters
@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_pwd(mythic_instance, task_timeout, callback_display_id):
    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="pwd",
        parameters="",
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert task["status"] in ["completed", "success"]
    assert len(output) > 0



@pytest.mark.os("Linux")
@pytest.mark.arch("amd64")
async def test_upload(mythic_instance, task_timeout, callback_display_id, tmp_path_factory):
    upload_path =  tmp_path_factory.getbasetemp() / str(uuid4())

    # Register a test file to upload
    resp = await mythic.register_file(
        mythic=mythic_instance, filename="test.txt", contents=b"this is a test"
    )

    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="upload",
        parameters={"path": str(upload_path), "filename": "test.txt"},
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert os.path.isfile(upload_path)
    os.remove(upload_path)
    assert len(output) > 0