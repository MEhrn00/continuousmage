from mythic import mythic
import asyncio
import pytest
import json


@pytest.mark.build
@pytest.mark.parametrize("selected_os", ["macOS"])
async def test_builds(mythic_instance, build_timeout, selected_os, operating_systems):
    if len(operating_systems) > 0 and selected_os not in operating_systems:
        pytest.skip(f"{selected_os} is not part of the selected operating systems: {operating_systems}")
    with open(f"./apfell/supporting-files/apfell_build.json", "r") as f:
        agent_config = json.load(f)
        # update the build args to replace architecture with the parameterized version
        updated_description = f"apfell build tests: {selected_os}"
        payload_response = await mythic.create_payload(
            mythic=mythic_instance,
            payload_type_name=agent_config["payload_type"],
            filename=agent_config["filename"],
            operating_system=selected_os,
            commands=agent_config["commands"],
            c2_profiles=agent_config["c2_profiles"],
            build_parameters=agent_config["build_parameters"],
            description=updated_description,
            return_on_complete=True,
            timeout=build_timeout,
            custom_return_attributes="""
                build_phase
                uuid
                build_stderr
                build_stdout
            """
        )
        await asyncio.sleep(2)
        assert payload_response["build_phase"] == "success"
